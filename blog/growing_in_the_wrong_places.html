<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <meta name="date" content='2023-07-02'>
    <link rel="stylesheet" type="text/css" href="/lib/css/normalize.css">
    <link rel="stylesheet" type="text/css" href="/lib/css/style.css">
    <link rel="icon" href="/lib/images/me.png">
    <link rel="alternate" type="application/atom+xml" title="blog" href="https://proto.garden/rss/blog.xml">
    <link rel="webmention" href="https://webmention.io/proto.garden/webmention">
    <meta name="theme-color" content="#7bad9f">
    <meta name="keywords" content="mental-health, personal, burnout">
    <title>proto's blog - Growing in the Wrong Places</title>
</head>

<body>
    <header>
        <img src="/lib/images/me.png"
            alt="a figure wearing a shirt and scarf with a floating crystalline head">
        <h1>proto ⋄</h1>
        <nav>
            <a href="./">up</a>
            <a href="/">back to homepage</a>
            <a href="/rss/blog.xml">rss</a>
        </nav>
        <em>originally written 2023-07-02</em>
    </header>
    <main>
        <h1 id="growing-in-the-wrong-places">Growing in the Wrong
        Places</h1>
        <p>The city that I live in used to be a steel production town,
        it has since morphed into a hybrid college town and regular
        small city, but the scars are still here. An environmental
        survey of a pond near my house revealed that the only way to
        clean it would be to either cover the whole thing over and
        forget it existed, or dig out &gt;6 feet of contaminated sludge
        from the floor of the pond. There isn’t 0 green-space in the
        city, but there is at least a 3:1 ratio of abandoned parking
        lots to flora. Nature within the city is often seen as a problem
        that needs to be overcome, rather than a harmonious
        relationship. In spite of this, small plants jut through cracks
        in the sidewalk made by the roots of trees. Squirrels use the
        power lines in lieu of dense forests, and geese inhabit the
        parks in droves, daring anybody to challenge their
        authority.</p>
        <p>College not only left me with burnout, but also a
        greatly-increased insecurity with my skills… especially the
        technical ones. Shortly after leaving, I attempted to hop back
        into my old habits of taking on freelance job after freelance
        job, pushing myself to learn new skills while being expected to
        complete tasks with those skills simultaneously. It didn’t
        stick, I left my most recent freelance job as the demands for
        the project changed beyond my scope, and began getting toxic
        messages from the client, explaining how I used to be someone
        she respected and that none of the work I had done mattered. I
        ended up paying her half of the 50% I was paid upfront just to
        be done with it, which left me unable to pay my bills and is a
        financial loss I am still recovering from. It was gutting, I
        felt like the last 2 years of my life had amounted to nothing,
        and left me in a worse place mentally and intellectually than I
        was when I went into college. I seriously considered throwing in
        the towel and abandoning technical work all together, the last 2
        years worth of projects leaving me with next to nothing I was
        proud of.</p>
        <p>I’ve been reading “<a
        href="https://codeberg.org/proto-0/protonexus/library/book/the_dawn_of_everything">The
        Dawn of Everything</a>” by the two Davids: Wengrow and Graeber.
        The most fascinating part to me is the descriptions of
        alternative methods of societal organization. Tales of
        structures and power dynamics ebbing and flowing alongside the
        seasons fills me with both newfound vindication that the world
        can be better, but also melancholy for freedom that I will never
        have. As I ruminated further, I became wary of taking a
        rose-tinted orientalist view of ancient societies, and it has
        required re-examination of the way I think about the experiences
        of others past and present.</p>
        <p>It is a reaction that feels reductive once I examine it
        further, at times sounding only slightly more nuanced than
        claiming I was “born in the wrong generation”. I have attempted
        to redirect the yearning into a motivation to find stability so
        that I may help others. The societies of millennia long past
        exist as cracks in the immovable veneer of society, proof that
        the structures of humanity are built and can be meaningfully
        changed by the people within them. It is tempting to want to
        imagine how things can be improved if given a fresh start, but
        as Marx says, we make our history “under self-selected
        circumstances, but under circumstances existing already, given
        and transmitted from the past”.</p>
        <p>With all of my friends nearing the end of their college
        experiences, I feel pangs of regret for the way the last 2 years
        went. Retroactively, I feel that I could have pushed myself
        harder, focused less on things that didn’t help me or bring me
        any happiness, and attempted to work on my mental health before
        it reached a boiling point. I didn’t do any of those things,
        though, and that regret only serves to overlay my vision of the
        future with a ghostly view of what could have been.</p>
        <p>While I wasn’t programming, I began reading more. <a
        href="https://tilde.town/~dozens/sofa/">Unshackled</a> by the
        need to finish every book I pick up, I have flipped through “Our
        Word is our Weapon”, “How Architecture Works: A Humanist’s
        Toolkit” (thanks to a thoughtful gift from my friend Skye), “The
        Sphere and the Labyrinth”, and for the second time “Piranesi”. I
        have reworked my notes, began working on a long term world
        building project (which I hope to share a piece of with you
        soon), improved my mental health, and began to gain a clearer
        understanding of the type of person I want to be in the
        future.</p>
        <p>The plants in the forest have far better prospects than the
        dandelions I see gasping for air amidst a sea of concrete and
        chemical waste, but that does not make the growth of the
        dandelion worthless. To stand firm and grow in the areas that
        you need allows an intricate web of skills and ideas that will
        be a foundation for all of your growth to come. For so long, I
        threw myself into the skills I had an interest in that I thought
        would be more profitable of valuable (whatever that means) in
        lieu of the things that made me happy. I tricked myself into
        thinking that there were no other options, or that making major
        shifts in my life would have all the grace of a barge doing a
        three-point turn.</p>
        <p>I have made very little progress in any of the skills I have
        that “matter” recently. My prospects are almost certainly made
        worse by me taking an unconventional path. I still struggle in a
        lot of the same ways I did when I was 18, but not all of them.
        Seeing the cracks in the monolithic definition of success has
        allowed me to take root in a new place with new ideas and new
        goals. I have been able to break out of the labels I prescribed
        to myself out of convenience when I was a very different person.
        I have decided to pursue the work that makes me happy and allows
        me to learn in an environment that doesn’t hurt me.</p>
        <p>With this in mind, I’m going to keep growing in all the wrong
        places, making my mark on a world that does not wish to see me
        succeed, and finding true joy. If I am lucky, I will one day be
        able to sit in the grass of a park that doesn’t want me to be
        there, honking at anyone to dares to get me to leave.</p>
    </main>
    
    <footer>
        <p>
            <b>♥ proto</b>
            <a href="https://merveilles.town/@proto" rel="me" target="_blank">
                <svg width="30" height="30" fill="#8eb29a" stroke="none" viewbox="60 60 200 200"
                    xmlns="http://www.w3.org/2000/svg">
                    <title>proto on mastodon</title>
                    <g>
                        <path
                            d="M185,65 A60,60 0 0,0 125,125 L185,125 Z M125,245 A60,60 0 0,0 185,185 L125,185 Z M95,125 A30,30 0 0,1 125,155 A30,30 0 0,1 95,185 A30,30 0 0,1 65,155 A30,30 0 0,1 95,125 Z M155,125 A30,30 0 0,1 185,155 A30,30 0 0,1 155,185 A30,30 0 0,1 125,155 A30,30 0 0,1 155,125 Z M215,125 A30,30 0 0,1 245,155 A30,30 0 0,1 215,185 A30,30 0 0,1 185,155 A30,30 0 0,1 215,125 " />
                        <path
                            d="M125,65 A60,60 0 0,1 185,125 L125,125 Z M185,245 A60,60 0 0,1 125,185 L185,185 Z M65,65 A60,60 0 0,1 125,125 L65,125 Z M65,245 A60,60 0 0,0 125,185 L65,185 Z M245,65 A60,60 0 0,0 185,125 L245,125 Z M245,245 A60,60 0 0,1 185,185 L245,185 Z" />
                    </g>
                </svg>
            </a>
            <a href="https://creativecommons.org/licenses/by-nc-sa/4.0/" target="_blank">
                <svg viewbox="0 0 30 30" fill="#8eb29a" style="width: 2rem;" xmlns="http://www.w3.org/2000/svg">
                    <title>creative commons 4.0</title>
                    <path
                        d="M14.972 0c4.196 0 7.769 1.465 10.715 4.393A14.426 14.426 0 0128.9 9.228C29.633 11.04 30 12.964 30 15c0 2.054-.363 3.978-1.085 5.772a13.77 13.77 0 01-3.2 4.754 15.417 15.417 0 01-4.983 3.322A14.932 14.932 0 0114.973 30c-1.982 0-3.88-.38-5.692-1.14a15.087 15.087 0 01-4.875-3.293c-1.437-1.437-2.531-3.058-3.281-4.862A14.71 14.71 0 010 15c0-1.982.38-3.888 1.138-5.719a15.062 15.062 0 013.308-4.915C7.303 1.456 10.812 0 14.972 0zm.055 2.706c-3.429 0-6.313 1.196-8.652 3.589a12.896 12.896 0 00-2.72 4.031 11.814 11.814 0 00-.95 4.675c0 1.607.316 3.156.95 4.646a12.428 12.428 0 002.72 3.992 12.362 12.362 0 003.99 2.679c1.483.616 3.037.924 4.662.924 1.607 0 3.164-.312 4.675-.937a12.954 12.954 0 004.084-2.705c2.339-2.286 3.508-5.152 3.508-8.6 0-1.66-.304-3.231-.91-4.713a11.994 11.994 0 00-2.651-3.965c-2.412-2.41-5.314-3.616-8.706-3.616zm-.188 9.803l-2.01 1.045c-.215-.445-.477-.758-.79-.937-.312-.178-.602-.268-.87-.268-1.34 0-2.01.884-2.01 2.652 0 .803.17 1.446.509 1.928.34.482.84.724 1.5.724.876 0 1.492-.43 1.85-1.286l1.847.937a4.407 4.407 0 01-1.634 1.728c-.696.42-1.464.63-2.303.63-1.34 0-2.42-.41-3.242-1.233-.821-.82-1.232-1.964-1.232-3.428 0-1.428.416-2.562 1.246-3.401.83-.84 1.879-1.26 3.147-1.26 1.858 0 3.188.723 3.992 2.17zm8.652 0l-1.983 1.045c-.214-.445-.478-.758-.79-.937-.313-.178-.613-.268-.897-.268-1.34 0-2.01.884-2.01 2.652 0 .803.17 1.446.51 1.928.338.482.838.724 1.5.724.874 0 1.49-.43 1.847-1.286l1.875.937a4.606 4.606 0 01-1.66 1.728c-.696.42-1.455.63-2.277.63-1.357 0-2.441-.41-3.253-1.233-.814-.82-1.22-1.964-1.22-3.428 0-1.428.415-2.562 1.246-3.401.83-.84 1.879-1.26 3.147-1.26 1.857 0 3.18.723 3.965 2.17z" />
                </svg>
            </a>
            <a href="https://webring.xxiivv.com/#proto" target="_blank" rel="noopener"
                style="background-color:var(--color-background);">
                <svg class="webring" xmlns="http://www.w3.org/2000/svg" width="48.876" height="43.963" fill="none" style="max-width:2rem;"
                    stroke="#8eb29a" stroke-linecap="square" stroke-width="28" version="1.1"
                    viewbox="0 0 97.752 57.925">
                    <title>merveilles webring</title>
                    <path d="M 71.956015,68.911383 A 24.94905,24.94905 0 1 0 28.742895,43.962503 L 7.9521752,79.973293"
                        style="stroke-width:11.6428" />
                    <path d="m 28.742895,68.911383 a 24.94905,24.94905 0 1 0 43.21312,-24.94888 L 51.165285,7.9517227"
                        style="stroke-width:11.6428" />
                    <path d="m 50.349455,31.488073 a 24.948873,24.948873 0 1 0 0,49.89774 h 41.58146"
                        style="stroke-width:11.6428" />
                </svg>
            </a>
            <a href="https://femishonuga.com/webring.html">
                <img src="/lib/images/aelrsty.png" alt="an abstract floral design" title="aelrsty webring" style="width:3rem;">
            </a>
            <em style="float: right; align-items: center; font-size: var(--font-size-base);">last updated: 2024-07-06</em>
        </p>
    </footer>
</body>

</html>
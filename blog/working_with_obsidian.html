<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <meta name="date" content='2023-07-05'>
    <link rel="stylesheet" type="text/css" href="/lib/css/normalize.css">
    <link rel="stylesheet" type="text/css" href="/lib/css/style.css">
    <link rel="icon" href="/lib/images/me.png">
    <link rel="alternate" type="application/atom+xml" title="blog" href="https://proto.garden/rss/blog.xml">
    <link rel="webmention" href="https://webmention.io/proto.garden/webmention">
    <meta name="theme-color" content="#7bad9f">
    <meta name="keywords" content="configuration, organization,
knowledge-management">
    <title>proto's blog - How I Take Notes with Obsidian</title>
</head>

<body>
    <header>
        <img src="/lib/images/me.png"
            alt="a figure wearing a shirt and scarf with a floating crystalline head">
        <h1>proto ⋄</h1>
        <nav>
            <a href="./">up</a>
            <a href="/">back to homepage</a>
            <a href="/rss/blog.xml">rss</a>
        </nav>
        <em>originally written 2023-07-05</em>
    </header>
    <main>
        <h1 id="how-i-take-notes-with-obsidian">How I Take Notes with
        Obsidian</h1>
        <p>I got interested in note-taking systems in 2020, and started
        seriously doing it a few months later. Looking back, I can now
        tell it was when I was seeing my grandma suffering with the
        early stages of dementia, and that really scared me. That loss
        of identity is something that I find existentially terrifying,
        and has been a motivating factor to take notes seriously. Now I
        enjoy taking notes for the sake of it, and continuously
        improving at it and seeing the growth of my vault of knowledge
        is both satisfying and helpful when I’m learning new skills.</p>
        <p>I never really learned how to take notes in school, and
        didn’t get good at it until after I dropped out. I am getting to
        a system that I find really useful and easy to work with. I’ve
        had about 3 distinct “eras” of <a
        href="https://codeberg.org/proto-0/protonexus">my obsidian
        notes</a>, and each time I have learned something more about the
        way that I want to organize my thoughts and notes. There’s no
        perfect way to take notes, and more than anything I recommend
        just doing it, and you will figure out over time what works and
        what doesn’t.</p>
        <p>I have to preface this by saying that obsidian has changed
        considerably over the past couple years. When I started using it
        there was no monetization system other than the option to just
        give them $20, now they offer both syncing between devices and
        publishing your vault for monthly fees. I don’t use those (I use
        <a href="https://syncthing.net/">syncthing</a> for syncing and
        will get into publishing a bit later), so I can’t speak to that.
        If any of the more abstract non text-file things obsidian does
        discourage you, then I would suggest finding a different tool
        like vimwiki that will suit your needs more. For me, obsidian is
        the easiest way to capture ideas, journal, and organize my
        thoughts and things I’ve learned. I was inspired by Winnie Lim’s
        <a
        href="https://winnielim.org/journal/experimenting-with-obsidian/">post</a>
        about starting obsidian to document the way I use obsidian in
        case it’s helpful for anyone else.</p>
        <h2 id="mindset">Mindset</h2>
        <p>Taking notes seems like a very straightforward thing, but if
        you struggle with a desire to have everything perfect and strive
        towards some imaginary “optimal” note-taking system, you are
        doomed. You have to be ok with notes being incomplete, outdated,
        or in progress. I also caution against trying to notate every
        thought and thing you learn, which is what I fell into when I
        started using obsidian a lot. You ultimately want your notes to
        be something that you refer back to, and so your notes should be
        something that you want to read.</p>
        <p>I also got way into the atomic note idea of each note being a
        single piece of information, but have since gravitated away from
        that because it makes the notes a bit less readable or something
        I was regularly going back to. I know a lot of people really
        like it, but it just wasn’t for me. To develop a system you
        really like, I recommend looking at how you have historically
        enjoyed writing or taking notes on things (if you have in the
        past) and figure out how to mimic that, rather than researching
        note-taking a ton and trying to fit your brain into someone
        else’s system.</p>
        <h2 id="some-tips">Some Tips</h2>
        <p>You can link to specific headings within your wikilinks like:
        [[working_with_obsidian#Tips]]</p>
        <p>You can resize images similarly (for some reason this doesn’t
        work for videos) ![[image|width]]</p>
        <p>Control-p is your friend, it is useful for both interacting
        with core obsidian functions and also most of the functionality
        within extensions. If you’re like me, and you suck at
        remembering hotkeys unless you use them all the time, this is a
        useful substitute.</p>
        <p>Don’t configure things in anticipation of how you will use
        them. You will notice friction points as you get more serious
        taking notes, and then you can grow into systems.</p>
        <h2 id="extensions">Extensions</h2>
        <p>I try to keep my notes themselves as close to raw markdown as
        possible for portability and ease of conversion to HTML. I’ve
        messed around with dataview, admonition, and a few other popular
        extensions, but none of them really stuck. However, I use quite
        a few extensions that I think are helpful.</p>
        <h3 id="daily-notes">Daily Notes</h3>
        <p>This is what I use for my journaling, my templates are linked
        below if you are curious. I journal on average once every 2-3
        days (always aiming for daily), and it has become the single
        most beneficial thing I have done regarding note-taking. Keeping
        track of my thoughts, the things I’ve done, and what I’m
        grateful for has made me more introspective and in tune with my
        own emotions. There is an extension called periodic notes if you
        want more granular control or things like weekly notes, but the
        recently built in Daily Notes extension works well for me.</p>
        <h3 id="templater">Templater</h3>
        <p>Templates were also introduced as a core plugin, but I still
        use the Templater community extension because it allows setting
        some variables that will be automatically generated when I make
        a new note from the templates. Once the note is generated, its
        just markdown, but this is the closest to an extension that is
        locked into the obsidian ecosystem.</p>
        <h3 id="folder-notes">Folder Notes</h3>
        <p>This is a nice way to have a little index note to each of
        your folders if you are interested in organizing your notes that
        way. For example, when I am taking notes on a book now I always
        make a folder that is the name of the book with a folder note
        that is going to be the main thing I go back to. Then I can have
        notes that contain highlights or more specific thoughts.</p>
        <h3 id="note-refactor">Note Refactor</h3>
        <p>I’ve found it easier when taking notes to do it all in one
        note and then split it after the fact. This is a simple
        extension that will let you split your notes into multiple notes
        based on the headings. While I don’t use it a lot, it’s always
        nice when I need it.</p>
        <h3 id="linter">Linter</h3>
        <p>This is only really relevant if you care about consistent
        formatting, but it is a pretty cool extension that allows you to
        have consistent properties around your notes. I’ve been
        experimenting with adding YAML front matter to my notes to
        eventually be able to use it for better sorting later, and the
        option to make sure I have my tags and other things consistent
        within my vault.</p>
        <h3 id="advanced-tables">Advanced Tables</h3>
        <p>This is the only extension I would say is a must-have if you
        are working with tables in obsidian. It makes the experience so
        much better with navigation, creation, and organizing tables. I
        can’t even express how much better it is because I haven’t used
        the vanilla table system in years. Just do yourself a favor and
        install this, and you’ll thank yourself if you make a table
        ever.</p>
        <h3 id="checklist">Checklist</h3>
        <p>This is a great way to aggregate TODOs or footnotes or
        reminders throughout your vault and have them aggregated on the
        sidebar. There is a more advanced task management tool within
        obsidian if you feel that you are outgrowing the checklist
        extension, but I never really messed around with it beyond a
        surface level introduction.</p>
        <h3 id="smart-typography">Smart Typography</h3>
        <p>This is a really simple extension that will automatically
        convert things like three periods into ellipses … → … or two
        greater-than/less-than signs into guillemets &lt;&lt; → “ It’s a
        really simple way to make your notes look a bit nicer and I
        really enjoy the simple aesthetic of having special characters
        in my notes. This overlaps with Linter a bit, but I still think
        it’s nice to have.</p>
        <h3 id="languagetool-integration">LanguageTool Integration</h3>
        <p>The name says it all, it’s just a better way to check your
        grammar than the built-in spell check, my advice is to turn
        auto-check off and check manually through the command
        prompt.</p>
        <h2 id="publishing">Publishing</h2>
        <p>$8-10/mo to have my notes visible online is more than I can
        afford. I want to integrate my notes into my website, but don’t
        want it to rely on external services or excessive JavaScript /
        fancy effects. I found an extension called Webpage HTML Export
        that allows you to export your whole vault as HTML, and gives
        you quite a few options for configuration. I’m going to make a
        more detailed post about this later because I’m working on some
        shell scripts that will do some post-processing for my notes to
        make them a bit more organized and customizable, but for now I
        would recommend playing around with that extension and seeing if
        it is something you would be interested in.</p>
        <h2 id="notes-on-mobile">Notes on Mobile</h2>
        <p>I suck at taking notes with obsidian on my phone, so my
        system is much leaner there I put everything into a note called
        “scratch”, and then process it when I sync it to my computer.
        There are a variety of extensions for mobile note-taking in the
        obsidian app, but I cannot speak to them at all. An important
        thing to keep in mind is that a lot of extensions won’t work on
        mobile, so if you rely heavily on dataview or other heavy
        extensions, you will not get to access that on mobile.</p>
    </main>
    
    <footer>
        <p>
            <b>♥ proto</b>
            <a href="https://merveilles.town/@proto" rel="me" target="_blank">
                <svg width="30" height="30" fill="#8eb29a" stroke="none" viewbox="60 60 200 200"
                    xmlns="http://www.w3.org/2000/svg">
                    <title>proto on mastodon</title>
                    <g>
                        <path
                            d="M185,65 A60,60 0 0,0 125,125 L185,125 Z M125,245 A60,60 0 0,0 185,185 L125,185 Z M95,125 A30,30 0 0,1 125,155 A30,30 0 0,1 95,185 A30,30 0 0,1 65,155 A30,30 0 0,1 95,125 Z M155,125 A30,30 0 0,1 185,155 A30,30 0 0,1 155,185 A30,30 0 0,1 125,155 A30,30 0 0,1 155,125 Z M215,125 A30,30 0 0,1 245,155 A30,30 0 0,1 215,185 A30,30 0 0,1 185,155 A30,30 0 0,1 215,125 " />
                        <path
                            d="M125,65 A60,60 0 0,1 185,125 L125,125 Z M185,245 A60,60 0 0,1 125,185 L185,185 Z M65,65 A60,60 0 0,1 125,125 L65,125 Z M65,245 A60,60 0 0,0 125,185 L65,185 Z M245,65 A60,60 0 0,0 185,125 L245,125 Z M245,245 A60,60 0 0,1 185,185 L245,185 Z" />
                    </g>
                </svg>
            </a>
            <a href="https://creativecommons.org/licenses/by-nc-sa/4.0/" target="_blank">
                <svg viewbox="0 0 30 30" fill="#8eb29a" style="width: 2rem;" xmlns="http://www.w3.org/2000/svg">
                    <title>creative commons 4.0</title>
                    <path
                        d="M14.972 0c4.196 0 7.769 1.465 10.715 4.393A14.426 14.426 0 0128.9 9.228C29.633 11.04 30 12.964 30 15c0 2.054-.363 3.978-1.085 5.772a13.77 13.77 0 01-3.2 4.754 15.417 15.417 0 01-4.983 3.322A14.932 14.932 0 0114.973 30c-1.982 0-3.88-.38-5.692-1.14a15.087 15.087 0 01-4.875-3.293c-1.437-1.437-2.531-3.058-3.281-4.862A14.71 14.71 0 010 15c0-1.982.38-3.888 1.138-5.719a15.062 15.062 0 013.308-4.915C7.303 1.456 10.812 0 14.972 0zm.055 2.706c-3.429 0-6.313 1.196-8.652 3.589a12.896 12.896 0 00-2.72 4.031 11.814 11.814 0 00-.95 4.675c0 1.607.316 3.156.95 4.646a12.428 12.428 0 002.72 3.992 12.362 12.362 0 003.99 2.679c1.483.616 3.037.924 4.662.924 1.607 0 3.164-.312 4.675-.937a12.954 12.954 0 004.084-2.705c2.339-2.286 3.508-5.152 3.508-8.6 0-1.66-.304-3.231-.91-4.713a11.994 11.994 0 00-2.651-3.965c-2.412-2.41-5.314-3.616-8.706-3.616zm-.188 9.803l-2.01 1.045c-.215-.445-.477-.758-.79-.937-.312-.178-.602-.268-.87-.268-1.34 0-2.01.884-2.01 2.652 0 .803.17 1.446.509 1.928.34.482.84.724 1.5.724.876 0 1.492-.43 1.85-1.286l1.847.937a4.407 4.407 0 01-1.634 1.728c-.696.42-1.464.63-2.303.63-1.34 0-2.42-.41-3.242-1.233-.821-.82-1.232-1.964-1.232-3.428 0-1.428.416-2.562 1.246-3.401.83-.84 1.879-1.26 3.147-1.26 1.858 0 3.188.723 3.992 2.17zm8.652 0l-1.983 1.045c-.214-.445-.478-.758-.79-.937-.313-.178-.613-.268-.897-.268-1.34 0-2.01.884-2.01 2.652 0 .803.17 1.446.51 1.928.338.482.838.724 1.5.724.874 0 1.49-.43 1.847-1.286l1.875.937a4.606 4.606 0 01-1.66 1.728c-.696.42-1.455.63-2.277.63-1.357 0-2.441-.41-3.253-1.233-.814-.82-1.22-1.964-1.22-3.428 0-1.428.415-2.562 1.246-3.401.83-.84 1.879-1.26 3.147-1.26 1.857 0 3.18.723 3.965 2.17z" />
                </svg>
            </a>
            <a href="https://webring.xxiivv.com/#proto" target="_blank" rel="noopener"
                style="background-color:var(--color-background);">
                <svg class="webring" xmlns="http://www.w3.org/2000/svg" width="48.876" height="43.963" fill="none" style="max-width:2rem;"
                    stroke="#8eb29a" stroke-linecap="square" stroke-width="28" version="1.1"
                    viewbox="0 0 97.752 57.925">
                    <title>merveilles webring</title>
                    <path d="M 71.956015,68.911383 A 24.94905,24.94905 0 1 0 28.742895,43.962503 L 7.9521752,79.973293"
                        style="stroke-width:11.6428" />
                    <path d="m 28.742895,68.911383 a 24.94905,24.94905 0 1 0 43.21312,-24.94888 L 51.165285,7.9517227"
                        style="stroke-width:11.6428" />
                    <path d="m 50.349455,31.488073 a 24.948873,24.948873 0 1 0 0,49.89774 h 41.58146"
                        style="stroke-width:11.6428" />
                </svg>
            </a>
            <a href="https://femishonuga.com/webring.html">
                <img src="/lib/images/aelrsty.png" alt="an abstract floral design" title="aelrsty webring" style="width:3rem;">
            </a>
            <em style="float: right; align-items: center; font-size: var(--font-size-base);">last updated: 2024-06-08</em>
        </p>
    </footer>
</body>

</html>
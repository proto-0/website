<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <meta name="date" content='2024-05-08'>
    <link rel="stylesheet" type="text/css" href="/lib/css/normalize.css">
    <link rel="stylesheet" type="text/css" href="/lib/css/style.css">
    <link rel="icon" href="/lib/images/me.png">
    <link rel="alternate" type="application/atom+xml" title="notes" href="https://proto.garden/rss/notes.xml">
    <link rel="webmention" href="https://webmention.io/proto.garden/webmention">
    <meta name="theme-color" content="#7bad9f">
    <meta name="keywords" content="pedagogy, theory, sociology">
    <title>proto's notes - The Usefulness of Useless Knowledge</title>
</head>

<body>
    <header>
        <img src="/lib/images/me.png"
            alt="a figure wearing a shirt and scarf with a floating crystalline head">
        <h1>proto ⋄</h1>
        <nav>
            <a href="./">up</a>
            <a href="/">back to homepage</a>
            <a href="/rss/notes.xml">rss</a>
        </nav>
        <em>last updated: 2024-11-05</em>

    </header>
    
    <main>
        <h1 id="the-usefulness-of-useless-knowledge">The Usefulness of
        Useless Knowledge</h1>
        <h2 id="abraham-flexner-for-harpers-magazine">Abraham Flexner
        for Harper’s Magazine</h2>
        <p>I was especially struck by the opening sentence and a later
        paragraph on the same page:</p>
        <blockquote>
        <p>“Is it not a curious fact that in a world steeped in
        irrational hatreds which threaten civilization itself, men and
        women-old and young-detach themselves wholly or partly from the
        angry current of daily life to devote themselves to the
        cultivation of beauty, to the extension of knowledge, to the
        cure of disease, to the amelioration of suffering, just as
        though fanatics were not simultaneously engaged in spreading
        pain, ugliness, and suffering?”</p>
        </blockquote>
        <blockquote>
        <p>“I have no quarrel with this tendency. The world in which we
        live is the only world about which our senses can testify.
        Unless it is made a better world, a fairer world, millions will
        continue to go to their graves silent, saddened, and embittered.
        I have myself spent many years pleading that our schools should
        become more acutely aware of the world in which their pupils and
        students are destined to pass their lives. Now I sometimes
        wonder whether that current has not become too strong and
        whether there would be sufficient opportunity for a full life if
        the world were emptied of some of the useless things that give
        it spiritual significance; in other words, whether our
        conception of what is useful may not have become too narrow to
        be adequate to the roaming and capricious possibilities of the
        human spirit.”</p>
        </blockquote>
        <p>Speaking of the invention of wireless networking through
        radio waves, Flexner recalls a conversation with a man named
        George Eastmen, who confidently asserted that Marconi was the
        most important of the pioneers in the field.</p>
        <blockquote>
        <p>“Hertz and Maxwell could invent nothing, but it was their
        useless theoretical work which was seized upon by a clever
        technician and which has created new means for communication,
        utility, and amusement by which men whose merits are relatively
        slight have obtained fame and earned millions. Who were the
        useful men? Not Marconi, but Clerk Maxwell and Heinrich Hertz.
        Hertz and Maxwell were geniuses without thought of use. Marconi
        was a clever inventor with no thought but use.”</p>
        </blockquote>
        <blockquote>
        <p>“Curiosity, which may or may not eventuate in something
        useful, is probably the outstanding characteristic of modern
        thinking. It is not new. It goes back to Galileo, Bacon, and to
        Sir Isaac Newton, and it must be absolutely unhampered.
        Institutions of learning should be devoted to the cultivation of
        curiosity and the less they are deflected by considerations of
        immediacy of application, the more likely they are to contribute
        not only to human welfare but to the equally important
        satisfaction of intellectual interest which may indeed be said
        to have become the ruling passion of intellectual life in modern
        times.”</p>
        </blockquote>
        <p>It’s really fascinating reading someone echoing a lot of what
        we discuss now in the kinds of socio-political discourse that
        pervades online, but that is born from a completely different
        context. Embroiled in post-WWI culture shock, the murderous
        potential that all inventions pose has been demonstrated in
        dynamite, the warplane, and the isolation of poisonous gasses
        for warfare. However, Flexner absolves the theoretical work of
        the scientists as innocent and in some ways inevitable. While I
        don’t entirely disagree I do not think that scientists can have
        totally unhampered creativity without moral and societal
        frameworks to discourage it’s misuse. I wonder if Flexner would
        write the same things after the allied powers became aware of
        the human experimentation of the Nazi Party, or of the dozens of
        scientists responsible for the Manhattan Project. Perhaps he
        would argue that this is symptomatic of the “results-based”
        scientific culture at the time, but I think it offers a vision
        into how complete curiosity cannot exist in a vacuum without
        some using it for immense harm.</p>
        <blockquote>
        <p>“I am not criticizing institutions like schools of
        engineering or law in which the usefulness motive necessarily
        predominates. Not infrequently the tables are turned, and
        practical difficulties encountered in industry or in
        laboratories stimulate theoretical inquiries which may or may
        not solve the problems by which they were suggested, but may
        also open up new vistas, useless at the moment, but pregnant
        with future achievements, practical and theoretical.”</p>
        </blockquote>
        <blockquote>
        <p>“it becomes obvious that one must be wary in attributing
        scientific discovery wholly to anyone person. Almost every
        discovery has a long and precarious history. Someone finds a bit
        here, another a bit there. A third step succeeds later and thus
        onward till a genius pieces the bits together and makes the
        decisive contribution. Science, like the Mississippi, begins in
        a tiny rivulet in the distant forest. Gradually other streams
        swell its volume. And the roaring river that bursts the dikes is
        formed from countless sources.”</p>
        </blockquote>
        <blockquote>
        <p>“In the face of the history of the human race what can be
        more silly or ridiculous than likes or dislikes founded upon
        race or religion? Does humanity want symphonies and paintings
        and profound scientific truth, or does it want Christian
        symphonies, Christian paintings, Christian science, or Jewish
        symphonies, Jewish paintings, Jewish science, or Mohammedan or
        Egyptian or Japanese or Chinese or American or German or Russian
        or Communist or Conservative contributions to and expressions of
        the infinite richness of the human soul?”</p>
        </blockquote>
        <p>The conclusion of this essay leads to Flexner waxing poetic
        about a group of “useless intellect” in the form of “The
        Institute” near Princeton.</p>
        <blockquote>
        <p>“The Institute must remain small; and it will hold fast to
        the conviction that The Institute Group desires leisure,
        security, freedom from organization and routine, and, finally,
        informal contacts with the scholars of Princeton University and
        others who from time to time can be lured to Princeton from
        distant places.”</p>
        </blockquote>
        <blockquote>
        <p>“We make ourselves no promises, but we cherish the hope that
        the unobstructed pursuit of useless knowledge will prove to have
        consequences in the future as in the past. Not for a moment,
        however, do we defend the Institute on that ground. It exists as
        a paradise for scholars who, like poets and musicians, have won
        the right to do as they please and who accomplish most when
        enabled to do so.”</p>
        </blockquote>
    </main>

    <footer>
        <p>
            <b>♥ proto</b>
            <a href="https://merveilles.town/@proto" rel="me" target="_blank">
                <svg width="30" height="30" fill="#8eb29a" stroke="none" viewbox="60 60 200 200"
                    xmlns="http://www.w3.org/2000/svg">
                    <title>proto on mastodon</title>
                    <g>
                        <path
                            d="M185,65 A60,60 0 0,0 125,125 L185,125 Z M125,245 A60,60 0 0,0 185,185 L125,185 Z M95,125 A30,30 0 0,1 125,155 A30,30 0 0,1 95,185 A30,30 0 0,1 65,155 A30,30 0 0,1 95,125 Z M155,125 A30,30 0 0,1 185,155 A30,30 0 0,1 155,185 A30,30 0 0,1 125,155 A30,30 0 0,1 155,125 Z M215,125 A30,30 0 0,1 245,155 A30,30 0 0,1 215,185 A30,30 0 0,1 185,155 A30,30 0 0,1 215,125 " />
                        <path
                            d="M125,65 A60,60 0 0,1 185,125 L125,125 Z M185,245 A60,60 0 0,1 125,185 L185,185 Z M65,65 A60,60 0 0,1 125,125 L65,125 Z M65,245 A60,60 0 0,0 125,185 L65,185 Z M245,65 A60,60 0 0,0 185,125 L245,125 Z M245,245 A60,60 0 0,1 185,185 L245,185 Z" />
                    </g>
                </svg>
            </a>
            <a href="https://creativecommons.org/licenses/by-nc-sa/4.0/" target="_blank">
                <svg viewbox="0 0 30 30" fill="#8eb29a" style="width: 2rem;" xmlns="http://www.w3.org/2000/svg">
                    <title>creative commons 4.0</title>
                    <path
                        d="M14.972 0c4.196 0 7.769 1.465 10.715 4.393A14.426 14.426 0 0128.9 9.228C29.633 11.04 30 12.964 30 15c0 2.054-.363 3.978-1.085 5.772a13.77 13.77 0 01-3.2 4.754 15.417 15.417 0 01-4.983 3.322A14.932 14.932 0 0114.973 30c-1.982 0-3.88-.38-5.692-1.14a15.087 15.087 0 01-4.875-3.293c-1.437-1.437-2.531-3.058-3.281-4.862A14.71 14.71 0 010 15c0-1.982.38-3.888 1.138-5.719a15.062 15.062 0 013.308-4.915C7.303 1.456 10.812 0 14.972 0zm.055 2.706c-3.429 0-6.313 1.196-8.652 3.589a12.896 12.896 0 00-2.72 4.031 11.814 11.814 0 00-.95 4.675c0 1.607.316 3.156.95 4.646a12.428 12.428 0 002.72 3.992 12.362 12.362 0 003.99 2.679c1.483.616 3.037.924 4.662.924 1.607 0 3.164-.312 4.675-.937a12.954 12.954 0 004.084-2.705c2.339-2.286 3.508-5.152 3.508-8.6 0-1.66-.304-3.231-.91-4.713a11.994 11.994 0 00-2.651-3.965c-2.412-2.41-5.314-3.616-8.706-3.616zm-.188 9.803l-2.01 1.045c-.215-.445-.477-.758-.79-.937-.312-.178-.602-.268-.87-.268-1.34 0-2.01.884-2.01 2.652 0 .803.17 1.446.509 1.928.34.482.84.724 1.5.724.876 0 1.492-.43 1.85-1.286l1.847.937a4.407 4.407 0 01-1.634 1.728c-.696.42-1.464.63-2.303.63-1.34 0-2.42-.41-3.242-1.233-.821-.82-1.232-1.964-1.232-3.428 0-1.428.416-2.562 1.246-3.401.83-.84 1.879-1.26 3.147-1.26 1.858 0 3.188.723 3.992 2.17zm8.652 0l-1.983 1.045c-.214-.445-.478-.758-.79-.937-.313-.178-.613-.268-.897-.268-1.34 0-2.01.884-2.01 2.652 0 .803.17 1.446.51 1.928.338.482.838.724 1.5.724.874 0 1.49-.43 1.847-1.286l1.875.937a4.606 4.606 0 01-1.66 1.728c-.696.42-1.455.63-2.277.63-1.357 0-2.441-.41-3.253-1.233-.814-.82-1.22-1.964-1.22-3.428 0-1.428.415-2.562 1.246-3.401.83-.84 1.879-1.26 3.147-1.26 1.857 0 3.18.723 3.965 2.17z" />
                </svg>
            </a>
            <a href="https://webring.xxiivv.com/#proto" target="_blank" rel="noopener"
                style="background-color:var(--color-background);">
                <svg class="webring" xmlns="http://www.w3.org/2000/svg" width="48.876" height="43.963" fill="none" style="max-width:2rem;"
                    stroke="#8eb29a" stroke-linecap="square" stroke-width="28" version="1.1"
                    viewbox="0 0 97.752 57.925">
                    <title>merveilles webring</title>
                    <path d="M 71.956015,68.911383 A 24.94905,24.94905 0 1 0 28.742895,43.962503 L 7.9521752,79.973293"
                        style="stroke-width:11.6428" />
                    <path d="m 28.742895,68.911383 a 24.94905,24.94905 0 1 0 43.21312,-24.94888 L 51.165285,7.9517227"
                        style="stroke-width:11.6428" />
                    <path d="m 50.349455,31.488073 a 24.948873,24.948873 0 1 0 0,49.89774 h 41.58146"
                        style="stroke-width:11.6428" />
                </svg>
            </a>
            <a href="https://femishonuga.com/webring.html">
                <img src="/lib/images/aelrsty.png" alt="an abstract floral design" title="aelrsty webring" style="width:3rem;">
            </a>
        </p>
    </footer>
</body>

</html>